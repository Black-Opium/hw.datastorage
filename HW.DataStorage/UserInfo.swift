//
//  UserInfo.swift
//  HW.DataStorage
//
//  Created by Black Opium on 13/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import Foundation

class UserInfo {
    static var shared = UserInfo()
    
    private let kUserNameKey:String = "UserInfo.name"
    private let kUserSurnameKey:String = "UserInfo.surname"

    var name: String? {
        set { PersistenceService.userDefaults.set(newValue, forKey: kUserNameKey) }
        get { return PersistenceService.userDefaults.string(forKey: kUserNameKey) }
    }
    var surname: String? {
        set { PersistenceService.userDefaults.set(newValue, forKey: kUserSurnameKey) }
        get { return PersistenceService.userDefaults.string(forKey: kUserSurnameKey) }
    }
}
