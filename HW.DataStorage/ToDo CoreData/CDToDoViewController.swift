//
//  CDToDoViewController.swift
//  HW.DataStorage
//
//  Created by Black Opium on 14/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import UIKit
import CoreData

protocol CDToDoViewControllerDelegate {
    func updateTableData()
}

class CDToDoViewController: UIViewController {

    var toDoList: [Tasks]?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTableData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CDShowEditor" {
            let vc = segue.destination as! CDEditorViewController
            vc.delegate = self
        } else if segue.identifier == "CDShowView" {
            let vc = segue.destination as! CDDisplayViewController
            let indexPath: IndexPath = tableView.indexPathForSelectedRow!
            vc.header = toDoList![indexPath.row].name
            vc.textContainer = toDoList![indexPath.row].text
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}

extension CDToDoViewController: CDToDoViewControllerDelegate {
    func updateTableData() {
        let request: NSFetchRequest<Tasks> = Tasks.fetchRequest()
        toDoList = nil
        toDoList = try! PersistenceService.CoreDateHandler.context.fetch(request).reversed()
        tableView.reloadData()
    }
}

extension CDToDoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = toDoList?.count {
            return count
        } else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath)
        cell.textLabel?.text = toDoList![indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "CDShowView", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let item = toDoList?[indexPath.row] {
                item.removeInCD()
                toDoList?.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
}
