//
//  CDEditorViewController.swift
//  HW.DataStorage
//
//  Created by Black Opium on 14/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import UIKit

class CDEditorViewController: UIViewController {
    
    var delegate: CDToDoViewControllerDelegate!
    
    @IBOutlet weak var taskName: UITextField!
    @IBOutlet weak var taskText: UITextView!
    
    @IBAction func createTask(_ sender: Any) {
        guard taskName.text != "" && taskText.text != "" else {
            return
        }
        
        let task = Tasks(context: PersistenceService.CoreDateHandler.context)
        task.name = taskName.text
        task.text = taskText.text
        task.date = NSDate()
        
        PersistenceService.CoreDateHandler.saveContext()
        
        delegate.updateTableData()
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTask(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
