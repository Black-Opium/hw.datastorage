//
//  Tasks+CoreDataClass.swift
//  HW.DataStorage
//
//  Created by Black Opium on 14/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Tasks)
public class Tasks: NSManagedObject {
    func removeInCD() {
        PersistenceService.CoreDateHandler.context.delete(self)
        try! PersistenceService.CoreDateHandler.context.save()
    }
}
