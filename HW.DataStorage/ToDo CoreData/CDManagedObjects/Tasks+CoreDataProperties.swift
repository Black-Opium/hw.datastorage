//
//  Tasks+CoreDataProperties.swift
//  HW.DataStorage
//
//  Created by Black Opium on 14/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//
//

import Foundation
import CoreData


extension Tasks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tasks> {
        return NSFetchRequest<Tasks>(entityName: "Tasks")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var name: String?
    @NSManaged public var text: String?

}
