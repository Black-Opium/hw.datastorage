//
//  WeatherViewController.swift
//  HW.WeatherAPI
//
//  Created by Black Opium on 12/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import UIKit
import RealmSwift
import SVProgressHUD

class WeatherViewController: UIViewController {
    
    var weatherData: [Weather]?
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func loadNewData(_ sender: Any) {
        SVProgressHUD.show()
        
        Weather.getFromNet { (result) in
            
            self.weatherData = result
            
            self.tableView.reloadData()
            
            SVProgressHUD.dismiss()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        
        Weather.getFromRealm { (result) in
            if result == nil {
                Weather.getFromNet { (resultFromNet) in
                    
                    self.weatherData = resultFromNet
                    
                    self.tableView.reloadData()
                    
                    SVProgressHUD.dismiss()
                }
            } else {
                self.weatherData = result
                
                self.tableView.reloadData()
                
                SVProgressHUD.dismiss()
            }
        }
    }
}

extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if weatherData == nil {
            return 0
        } else {
            return weatherData!.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath)
        cell.textLabel?.text = weatherData![indexPath.row].date
        cell.detailTextLabel?.text = weatherData![indexPath.row].forecast
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

@IBDesignable extension UIButton {
    @IBInspectable var cornerRadius: CGFloat  {
        set {
            self.layer.cornerRadius = newValue
        }
        get {
            return self.layer.cornerRadius
        }
    }
}
