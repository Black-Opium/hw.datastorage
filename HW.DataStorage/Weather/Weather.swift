//
//  Weather.swift
//  HW.DataStorage
//
//  Created by Black Opium on 15/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import Foundation
import RealmSwift

class Weather: Object {
    
    @objc dynamic var date: String?
    dynamic var minTemp: Double?
    dynamic var maxTemp: Double?
    @objc dynamic var forecast: String?
    
    static func getFromNet( complition: @escaping ([Weather]) ->Void ) {
        JSONhandler.getJSONbyAlamofire { (result) in
            guard result != nil else {
                return
            }
            
            try! PersistenceService.RealmHandler.write {
                getFromRealm(complition: { (allData) in
                    if allData != nil  {
                        PersistenceService.RealmHandler.delete(allData!)
                    }
                    for item in result! {
                        PersistenceService.RealmHandler.add(item)
                    }
                })
            }
            
            complition(result!)
            return
        }
    }
    
    static func getFromRealm(complition: @escaping ([Weather]?) ->Void ) {
        let allObjects = PersistenceService.RealmHandler.objects(Weather.self)
        if allObjects.count != 0 {
            var weatherData: [Weather] = []

            for item in allObjects {
                weatherData.append(item)
            }
            
            complition(weatherData)
            return
        } else {
            complition(nil)
            return
        }
    }
}
