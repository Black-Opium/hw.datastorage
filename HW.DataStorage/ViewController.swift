//
//  ViewController.swift
//  HW.DataStorage
//
//  Created by Black Opium on 13/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var surnameField: UITextField!
    
    @IBAction func editingName(_ sender: Any) {
        UserInfo.shared.name = nameField.text
    }
    
    @IBAction func editingSurname(_ sender: Any) {
        UserInfo.shared.surname = surnameField.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameField.text = UserInfo.shared.name
        surnameField.text = UserInfo.shared.surname
    }


}

