//
//  JSONHandler.swift
//  HW.DataStorage
//
//  Created by Black Opium on 15/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import Foundation
import Alamofire

class JSONhandler {
    
    static func getJSONbyAlamofire(completion:@escaping ([Weather]?) -> Void) {
        let locationName: String = "?q=Moscow,ru"
        let units: String = "&units=metric"
        let key: String = "&appid=b370a0e82acf5e9e3e908d0c03862ff7"
        let url: URLConvertible = "https://api.openweathermap.org/data/2.5/forecast\(locationName + units + key)"
        
        //"https://api.openweathermap.org/data/2.5/forecast?q=Moscow,ru&units=metric&appid=b370a0e82acf5e9e3e908d0c03862ff7"
        
        AF.request(url, method: .get).validate().responseJSON(queue: .main, options: .allowFragments) { (response) in
            guard let value = response.value as? [String:Any?], let lists = value["list"] as? Array<[String:Any?]> else {
                completion(nil)
                return
            }

            var result:[Weather] = []

            for case let item in lists  {
                guard let main = item["main"] as? [String:Any?] else {
                    print("Section \"Main\" not found")
                    completion(nil)
                    continue
                }
                guard let interval = item["dt"] as? TimeInterval else {
                    print("Section \"Main\" not found")
                    completion(nil)
                    continue
                }
                guard let maxTemp = main["temp_max"] as? Double else {
                    print("Section \"temp_max\" not found")
                    completion(nil)
                    continue
                }
                guard let minTemp = main["temp_min"] as? Double else {
                    print("Section \"temp_min\" not found")
                    completion(nil)
                    continue
                }
                
                let weather: Weather = Weather()
                
                let dateForrmate: DateFormatter = DateFormatter()
                dateForrmate.dateFormat = "E, d MMM H:mm"
                dateForrmate.locale = Locale(identifier: "ru-RU")
                let dateOjc = Date(timeIntervalSince1970: interval)
                weather.date = dateForrmate.string(from: dateOjc)
                weather.minTemp = round(minTemp)
                weather.maxTemp = round(maxTemp)
                weather.forecast = String(format: "от %.0f до %.0f", weather.minTemp!, weather.maxTemp!) + " ℃"
                
                result.append(weather)
            }
            
            completion(result)
            
            return
        }
        /*
        AF.request(url, method: .get).validate().responseJSON { (response) in
            
            guard let value = response.result.value as? [String:Any?], let lists = value["list"] as? Array<[String:Any?]> else {
                completion(nil)
                return
            }
            
            var result:[Weather] = []
            
            for case let item in lists  {
                guard let main = item["main"] as? [String:Any?] else {
                    print("Section \"Main\" not found")
                    completion(nil)
                    continue
                }
                guard let interval = item["dt"] as? TimeInterval else {
                    print("Section \"Main\" not found")
                    completion(nil)
                    continue
                }
                guard let maxTemp = main["temp_max"] as? Double else {
                    print("Section \"temp_max\" not found")
                    completion(nil)
                    continue
                }
                guard let minTemp = main["temp_min"] as? Double else {
                    print("Section \"temp_min\" not found")
                    completion(nil)
                    continue
                }
                
                var weather: Weather = Weather()
                
                let dateForrmate: DateFormatter = DateFormatter()
                dateForrmate.dateFormat = "E, d MMM H:mm"
                dateForrmate.locale = Locale(identifier: "ru-RU")
                let dateOjc = Date(timeIntervalSince1970: interval)
                weather.date = dateForrmate.string(from: dateOjc)
                weather.minTemp = round(minTemp)
                weather.maxTemp = round(maxTemp)
                weather.forecast = String(format: "от %.0f до %.0f", weather.minTemp!, weather.maxTemp!) + " ℃"
                
                result.append(weather)
            }
            
            completion(result)
            
            return
        }
        */
    }

}
