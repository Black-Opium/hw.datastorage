//
//  ToDoList.swift
//  HW.DataStorage
//
//  Created by Black Opium on 13/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import Foundation
import RealmSwift

class Task: Object {
    
    @objc dynamic var name: String?
    @objc dynamic var text: String?
    @objc dynamic var date: Date?
    
    static func getData() -> [Task] {
        let data = PersistenceService.RealmHandler.objects(Task.self).sorted(byKeyPath: "date", ascending: false)
        var list: [Task] = []
        for item in data {
            list.append(item)
        }
        return list
    }
    
    func removeInRealm() {
        try! PersistenceService.RealmHandler.write {
            PersistenceService.RealmHandler.delete(self)
        }
    }
}
