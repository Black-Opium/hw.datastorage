//
//  EditorViewController.swift
//  HW.DataStorage
//
//  Created by Black Opium on 13/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import UIKit
import RealmSwift

class EditorViewController: UIViewController {
    
    var delegate: ToDoViewControllerDelegate!
    
    @IBOutlet weak var taskName: UITextField!
    @IBOutlet weak var taskText: UITextView!
    
    @IBAction func createTask(_ sender: Any) {
        guard taskName?.text != "" && taskName.text != ""  else {
            return
        }
        
        let task: Task = Task()
        task.name = taskName.text
        task.text = taskText.text
        task.date = Date()
        
        try! PersistenceService.RealmHandler.write {
            PersistenceService.RealmHandler.add(task)
        }
        
        delegate.updateTableData()
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTask(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
