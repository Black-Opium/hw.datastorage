//
//  DisplayViewController.swift
//  HW.DataStorage
//
//  Created by Black Opium on 15/09/2019.
//  Copyright © 2019 Black Opium. All rights reserved.
//

import UIKit

class DisplayViewController: UIViewController {
    
    var header: String? {
        didSet {
            navigationItem.title = header
        }
    }
    
    var textContainer: String?
    
    @IBOutlet weak var taskText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        taskText.text = textContainer
    }
    
}
